package com.example.demo.repository;

import org.springframework.data.repository.CrudRepository;

import com.example.demo.entity.CustomerType;


public interface CustomerTypeRepository  extends CrudRepository<CustomerType, Long>{

}
