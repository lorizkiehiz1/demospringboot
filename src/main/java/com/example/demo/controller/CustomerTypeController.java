package com.example.demo.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.CustomerType;
import com.example.demo.service.CustomerTypeService;

@RestController
@RequestMapping("/customertypes")
public class CustomerTypeController {
	
	@Autowired
	CustomerTypeService custTypeService;
	
    @GetMapping()
    public List<CustomerType> getCustomers() {
        return custTypeService.retrieveCustomerType();
    }
	
    @PostMapping()
    public ResponseEntity<?> postCustType(@Valid @RequestBody CustomerType body) {
    	CustomerType customer = custTypeService.create(body);
        return ResponseEntity.status(HttpStatus.CREATED).body(customer);
    }
}
