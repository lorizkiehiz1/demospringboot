package com.example.demo.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.CustomerType;
import com.example.demo.repository.CustomerTypeRepository;


@Service
public class CustomerTypeService {
	
	private CustomerTypeRepository customerTypeRepo;

	@Autowired
	public CustomerTypeService(CustomerTypeRepository customerTypeRepo) {
		this.customerTypeRepo = customerTypeRepo;
	}
	
    public List<CustomerType> retrieveCustomerType() {
        return (List<CustomerType>) customerTypeRepo.findAll();
    }
	
	public CustomerType create(CustomerType cusType) {
		cusType.setId(null);
		return customerTypeRepo.save(cusType);
	}
}
